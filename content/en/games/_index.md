
---
title: "Game projects"
linkTitle: "Game projects"
weight: 5
type: _default
menu:
  main:
    weight: 5
    pre: <i class='fas fa-gamepad'></i>
gamePage: false
---

{{< blocks/cover color="dark" height="min" >}}

<div class="col-12">
<h1 class="text-center">My recent game projects, sorted by date.</h1>
<p class="text-center">Go to Other projects for non-videogames projects.</p>
</div>

{{% /blocks/cover %}}

{{< blocks/section color="blue" >}}
## [Voidscape](voidscape/)
{{% /blocks/section %}}

{{< blocks/section color="green" >}}
<h2 class="text-center">Shroom</h2>
{{% /blocks/section %}}

{{< blocks/section color="yellow" >}}
<h2 class="text-center">NGC6705</h2>
{{% /blocks/section %}}

{{< blocks/section color="brown" >}}
<h2 class="text-center">Big Brain Tiles</h2>
{{% /blocks/section %}}

{{< blocks/section color="blue" >}}
<h2 class="text-center">Nyasteroids</h2>
{{% /blocks/section %}}

{{< blocks/section color="dark" >}}
<h2 class="text-center">Lost+Found</h2>
{{% /blocks/section %}}

{{< blocks/section color="red" >}}
<h2 class="text-center">NoVa Game</h2>
{{% /blocks/section %}}