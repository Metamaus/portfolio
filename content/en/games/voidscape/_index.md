---
title: "Voidscape"
linkTitle: "Voidscape"
weight: 20
type: _default
resources:
- src: "**.{png,jpg}"
---

{{< blocks/cover title="" image_anchor="bottom" height="medium" color="blue" >}}
<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="voidscape-logo" height="350" src="/images/voidscapelogo.png" />
    </p>
  </div>
</div> 

<p class="lead mt-5">A fast paced 3D parkour game with an original teleportation mechanic.</p>

{{< /blocks/cover >}}

{{< blocks/section color="dark" >}}
<div class="col-4 bg-light align-self-sm-start"> 
    <div class="text-dark">
        School Project<br>
        From october to december 2021<br>
        Unreal Engine<br>
        5 programmers
    </div> 
</div>
<div class="col-8 align-self-lg-center">
    <b>Voidscape</b> is a first person parkour game where you have to escape an unknown threat using a strange teleportation knife. You have to finish as fast as you can each of the 4 levels, using the environment and your capacities. In terms of Game design, it is meant to be fast-paced and precise, to allow players to try to perform as good as they can.
</div>
In this project, I focused mostly on visual aspects. I implemented the UI system along with the level management, trying to make it as easy to use as possible.
{{< /blocks/section >}}