---
title: Other Projects
linkTitle: Other Projects
menu:
    main:
        weight: 10
        pre: <i class="fas fa-file-code"></i>
gamePage: false
---
{{< blocks/cover color="dark" height="min" >}}
# My other Projects
{{% /blocks/cover %}}

{{< blocks/section color="pink" >}}
## C++ network library
{{% /blocks/section %}}

{{< blocks/section color="blue" >}}
## Jeco
{{% /blocks/section %}}