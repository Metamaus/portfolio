---
title: "NoVaGame"
linkTitle: "NoVaGame"
weight: 10
type: _default
resources:
- src: "**.{png,jpg}"
---

{{< blocks/cover title="" image_anchor="left" height="max" color="red" >}}
<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="Novagame" height="350" src="novalogo.png" />
    </p>
  </div>
</div>

[**page itch**](https://metamaus.itch.io/nova-game)
{{< /blocks/cover >}}

{{< blocks/section color="red" >}}
<div class="container-md">
    <div class="row align-items-center">
        <div class="col-10 text-center">
            <h4>
            Réalisé sur commande de juin à septembre 2020, <b>NoVaGame</b> est une visite virtuelle ludique de la salle de spectacles de la ville de Velaux, NoVa Velaux. Mon travail a consisté à implémenter l'ensemble du jeu sur Unity, seul le modèle 3d de la salle était déjà en grande partie réalisé.
            </h4>
        </div>
        <div class="col-2 p-3 mb-2 bg-primary text-right text-light">
            été 2020<br>
            <h4>Unity <img alt="voidscape-logo" height="20" src="LogoUnityClair.png" /><br></h4>
            Projet professionnel <br>
            1 personnes
        </div>
    </div>
    <div class="row">
    <h3>Game design</h3>
    <p>C'est un jeu d'énigmes et de collecte. Deux série d'énigmes permettent d'allumer la console de son et la console de lumières dans la salle de spectacle. On est aidé dans ces énigmes par des fiches-métiers à collecter, et différents points d'informations sur la salle en général.</p>
    </div>
    <div class="row">
    <h3>Programmation</h3>
    <p>En plus de devoir être jouable sur navigateur, le jeu devait être facilement adaptable, afin de pouvoir prendre en considération les modifications et adaptations demandées par les personnes travaillant à l'espace NoVa.</p>
    </div>
    <div class="row m-5">
        <div class="col-12 text-center showcase">
                <img class="img-fluid" alt="Une fiche métier" src="ficheMetier.png" />
                Récupération d'une fiche métier
        </div>
    </div>
</div>
{{< /blocks/cover >}}