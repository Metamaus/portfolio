
---
title: "Projets de jeux"
linkTitle: "Projets de jeux"
weight: 5
type: _default
menu:
  main:
    weight: 5
    pre: <i class='fas fa-gamepad'></i>
---

{{< blocks/cover color="dark" height="min" >}}

<div class="col-12">
<h1 class="text-center">Mes projets de jeux réalisés récemments.</h1>
<p class="text-center">Pour les projets qui ne sont pas des jeux vidéos, voir la section Autres projets.</p>
</div>

{{% /blocks/cover %}}

{{< blocks/section color="blue" >}}
<div class="row">
  <div class="col">
  <h2><a href="voidscape/">Voidscape</a></h2>
  <div>Un platformer rapide basé sur une mécanique de téléportation.</div>
  <a href="voidscape/">En savoir plus</a>
  </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="green" >}}
<div class="row">
  <div class="col">
  <h2><a href="ngc6705/">NGC6705</a></h2>
  <div>Un mini-jeu en réseau où deux canards ramassent des étoiles.</div>
  <a href="ngc6705/">En savoir plus</a>
  </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="red" >}}
<div class="row">
  <div class="col">
  <h2><a href="shroom/">Shroom</a></h2>
  <div>Un FPS en multijoueur réseau, où il faut coopérer pour éliminer une corruption rampante.</div>
  <a href="shroom/">En savoir plus</a>
  </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="green" >}}
<div class="row">
  <div class="col">
  <h2><a href="bigbraintiles/">Big Brain Tiles</a></h2>
  <div>Un jeu de placement de tuiles en versus.</div>
  <a href="bigbraintiles/">En savoir plus</a>
  </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="blue" >}}
<div class="row">
  <div class="col">
  <h2><a href="nyasteroids/">Nyasteroids</a></h2>
  <div>Un jeu d'arcade où un asteroïde répand la vie dans le cosmos.</div>
  <a href="nyasteroids/">En savoir plus</a>
  </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="dark" >}}
<div class="row">
  <div class="col">
  <h2><a href="lostfound/">Lost+Found</a></h2>
  <div>Un jeu d'énigme à l'ambiance sombre.</div>
  <a href="lostfound/">En savoir plus</a>
  </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="red" >}}
<div class="row">
  <div class="col">
  <h2><a href="novagame/">NoVaGame</a></h2>
  <div>Un jeu d'exploration pour découvrir une salle de spectacle et le milieu du théâtre.</div>
  <a href="novagame/">En savoir plus</a>
  </div>
</div>
{{% /blocks/section %}}