---
title: "Nyasteroids"
linkTitle: "Nyasteroids"
weight: 10
type: _default
resources:
- src: "**.{png,jpg}"
---

{{< blocks/cover title="" image_anchor="bottom" height="max" color="blue" >}}
<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="Nyasteroids" height="350" src="nyasteroidslogo.png" />
    </p>
  </div>
</div>

## [**page itch**](https://metamaus.itch.io/ngc6705)
{{< /blocks/cover >}}

{{< blocks/section color="blue">}}
<div class="container-md">
    <div class="row align-items-center">
        <div class="col-10 text-center">
            <h4>
            <b>Nyasteroids</b> est un jeu assez simple à propos d'un astéroïde cherchant à répandre une vie féline à travers le cosmos.
            Il a été réalisé pour une <b>MiniJam itch.io</b> et c'est un jeu sur lequel j'ai peu participé au code. Je me suis concentré sur la réalisation d'assets, d'animations et sur la coordination de l'équipe. Il est malgré tout dans ce portfolio car c'est mon projet de GameJam que je considère comme le plus abouti.
            </h4>
        </div>
        <div class="col-2 p-3 mb-2 bg-dark text-right text-light">
            juillet 2021<br>
            <h4>Godot<img alt="logo godot engine" height="20" src="LogoGodotClair.png" /><br></h4>
            GameJam <br>
            4 personnes
        </div>
    </div>
    <div class="row m-2">
        <div class="col-12 text-center">
        La contrainte de cette Jam était "Vous êtes vos propres munitions". C'est pourquoi l'astéroïde doit envoyer la vie qu'il contient afin de faire revivre les planètes mortes, et si il souhaite détruire les corps céleste dangereux, il doit envoyer sa propre matière rocheuse.
        Voici donc les différents états de notre "personnage" suivant son nombre de munitions:
        </div>
    </div>
    <div class="row m-3">
        <div class ="col-6 text-center">
            <img alt="Personnage au sommet de sa forme" class="img-fluid" src="fullfull.png" />
            <h4>Personnage plein de vie et de roche.</h4>
        </div>
        <div class ="col-6 text-center">
            <img alt="Personnage blessé mais contenant de la vie" class="img-fluid" src="fullhurt.png" />
            <h4>Peu de roche</h4>
        </div>
    </div>
    <div class="row m-3">
        <div class ="col-6 text-center">
            <img alt="Personnage en forme mais sans vie" class="img-fluid" src="emptyfull.png" />
            <h4>Peu de vie.</h4>
        </div>
        <div class ="col-6 text-center">
            <img alt="Personnage blessé et sans vie" class="img-fluid" src="emptyhurt.png" />
            <h4>Peu de roche et de vie.</h4>
        </div>
    </div>
</div>
{{< /blocks/section >}}