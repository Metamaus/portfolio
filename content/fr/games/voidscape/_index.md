---
title: "Voidscape"
linkTitle: "Voidscape"
weight: 20
type: _default
resources:
- src: "**.{png,jpg}"
menu:
    games:
        weight: 5
---

{{< blocks/cover title="" image_anchor="bottom" height="medium" color="blue" >}}
<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="voidscape-logo" height="350" src="voidscapelogo.png" />
    </p>
  </div>
</div>

[**page itch**](https://celestinml.itch.io/voidscape)

{{< /blocks/cover >}}

{{< blocks/section color="blue" >}}

<div class="row align-items-center">
    <div class="col-9 text-center">
        <h5>
        <b>Voidscape</b> est un jeu mêlant plateformer 3d et énigmes dans un univers aux inspirations cyberpunks. Il a été réalisé dans le cadre d'un cours à l'aide d'une équipe de 5 programmeurs.
        Je me suis investi dans le game design, l'interface utilisateur et la gestion des scènes et la modélisation et animation des ennemis.
        </h5>
    </div>
    <div class="col-3 p-3 mb-2 bg-dark text-right">
        d'octobre à décembre 2021<br>
        <h4>Unity <img alt="Logo unity" height="20" src="LogoUnityClair.png" /><br></h4>
        Projet de cours <br>
        5 personnes
    </div>
</div>

<div class="row">
    <div class="col-4">
        <h3><br>Game Design</h3>
        <div>Afin de pouvoir réaliser l'entièreté du projet dans le temps imparti, et de ne pas produire un jeu désagréable à jouer, nous avons décidé de réduire les mécaniques au minimum: déplacements, saut, sprint, téléportation. Nous avons ainsi pu passer beaucoup de temps à peaufiner le ressenti du jeu, et produire des niveaux agréables.</div> 
    </div>
    <div class="col-4">
        <h3><br> Gestion des scène et UI </h3>
        Je me suis principalement occupé de la gestion des scènes (sélection d'un niveau, mise en pause...) et de l'aspect visuel lié, l'UI. L'objectif tait d'avoir des outils pratiques pour changer de niveaux et afficher des informations, sans empiéter sur le jeu.
    </div>
    <div class="col-4">
        <p class="text-center showcase">
            <br>
            <img alt="voidscape-menu" class="img-fluid" src="voidscapemenu.png" />
            Menu de choix de niveau.
        </p>
    </div>
</div>

<div class="row">
    <div class="col-4">
        <p class="text-center showcase">
            <br>
            <img class="img-fluid" alt="voidscape-monsters" src="voidscapemonsters.png" />
            <br>
            Les ennemis: le "prisme" et le "bélier"
        </p>
    </div>
    <div class="col-7">
        <h3><br>Modélisation et animation des ennemis</h3>
        Comme nous avions choisit des décors très épurés, il a été très simple de modéliser et d'animer des adversaires s'intégrant bien dans ce thème.<br>
        Le <b>prisme</b> est un ennemi flottant, créant une zone d'effet autour de lui. Sa relative simplicité nous a servi de prototype pour vérifier qu'il était possible d'insérer des créatures animées par nos soins dans le projet.<br>
        Le <b>bélier</b> fut plus complexe à animer, et reste quelque peux raide mais son design simple a permis de le rendre assez convaincant.
    </div>
</div>
{{< /blocks/section >}}