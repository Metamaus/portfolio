---
title: "Big Brain Tiles"
linkTitle: "Big Brain Tiles"
weight: 10 
type: _default 
resources:
- src: "**.{png,jpg}"
---

{{< blocks/cover title="" image_anchor="bottom" height="max" color="yellow" >}}

<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="Logo big brain tiles" height="500" src="bbtlogo.png" />
    </p>
  </div>
</div>

{{< /blocks/cover >}}

{{< blocks/section color="orange" >}}
<div class="container-xxl">
    <div class="row align-items-center">
        <div class="col-9 text-center">
            <h5>
            <b>Big Brain Tiles</b> est un jeu réalisé lors de la <b>WonderJam</b> d'automne de l'UQAC, qui n'a malheureusement pas été complété durant cet intervalle de temps.
            Dans ce jeu, je me suis occupé de deux aspects: le game design, et la visualisation du plateau.
            </h5>
        </div>
        <div class="col-3 p-3 mb-2 bg-dark text-right text-light">
            octobre 2021<br>
            <h4>Unreal Engine 4 <img alt="voidscape-logo" height="20" src="LogoUnrealClair.png" /><br></h4>
            Game Jam <br>
            7 personnes
        </div>
    </div>
    <div class="row m-3">
        <div class="col-6 ">
            <h2>Game Design:</h2>  
            <div>Deux personnes s'affrontent sur une grille de 6x7 cases, et doivent jouer des cartes afin de relier leur base à des cristaux de pouvoir.<br>
            Ce fut très amusant de préparer un petit prototype en carton et de passer la première soirée à vérifier que notre idée était viable et à en équilibrer un peu les valeurs.
            </div>
        </div>
        <div class="col-6">
            <h2>Affichage de la grille:</h2>
            <div>Comme l'entièreté de la grille pouvait être amenée à changer au fur et à mesure des cartes posées, il a fallu générer la grille entièrement automatiquement, pour ensuite pouvoir changer l'appartenance de chaque case, dessiner des chemins ou faire apparaître les dits cristaux.</div>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <p class="text-center showcase">
                <img class="img-fluid" alt="Tuile de feu" height="350" src="fire.png" />
            </p>
        </div>
        <div class="col-4">
            <p class="text-center showcase">
                <img class="img-fluid" alt="Tuile neutre" height="350" src="empty.png" />
            </p>
        </div>
        <div class="col-4">
            <p class="text-center showcase">
                <img class="img-fluid" alt="Tuile d'eau" height="350" src="water.png" />
            </p>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-12 align-self-center">    
            <p class="text-center showcase">
                <b>Socle des différentes cases</b><br>
                complétés au besoin par des cristaux, ou modifiés pour tracer des chemins
            </p>
        </div>
    </div>
</div>
{{< /blocks/section >}}