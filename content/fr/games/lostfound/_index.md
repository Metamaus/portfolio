---
title: "Lost+Found"
linkTitle: "Lost+Found"
weight: 10
type: _default
resources:
- src: "**.{png,jpg}"
---

{{< blocks/cover title="" image_anchor="bottom" height="max" color="dark" >}}

<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="voidscape-logo" height="350" src="lostFoundlogo.png" />
    </p>
  </div>
</div>

[**page itch**](https://metamaus.itch.io/lostfound)

{{< /blocks/cover >}}

{{< blocks/section color="dark" >}}
<div class="row align-items-center">
    <div class="col-10 text-center">
        <h4>
        <b>Lost+Found</b> est un jeu d'ambiance horrifique, réalisé pour la GameJam ENSEIRB/MMI, organisée en parallèle de la <b>Global GameJam</b>. Durant ces 48h, je me suis principalement occupé de développer les mécaniques du jeu, ainsi que de l'UI.
        </h4>
    </div>
    <div class="col-2 p-3 mb-2 bg-primary text-right text-light">
        janvier 2021<br>
        <h4>Unity <img alt="voidscape-logo" height="20" src="LogoUnityClair.png" /><br></h4>
        GameJam <br>
        6 personnes
    </div>
</div>
<div class="row">
Outre les déplacements du personnage, la mécanique principale de ce jeu consiste en un système de "hack". Il est possible de rassembler dans le niveau des fonctionnalités. Ces dernières sont ensuite applicables aux objets mécaniques (ici un ascenceur et sa porte), afin de les utiliser de la manière voulue.<br>
Deux fonctionnalités ont été implémentées: Ouvrir et Activer. Cela permet d'activer la porte de l'ascenceur central (qui est éteinte), de l'ouvrir et de la désactiver afin qu'elle ne se referme plus.
</div>
<div class="row">
    <div class="col-12 text-center showcase">
            <img class="img-fluid" alt="Le menu de hack" src="UI.png" />
            UI permettant de "hacker" l'ascenceur
    </div>
</div>
{{< /blocks/section >}}