---
title: "Shroom"
linkTitle: "Shroom"
weight: 10
type: _default
resources:
- src: "**.{png,jpg}"
---

{{< blocks/cover title="" image_anchor="bottom" height="medium" color="red" >}}
<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="voidscape-logo" height="350" src="shroomlogo.png" />
    </p>
  </div>
</div>
{{< /blocks/cover >}}

{{< blocks/section color="red" >}}
<div class="container-md">
    <div class="row">
        <div class="col-9 text-center">
            <h5>
                <b>Shroom</b> est un projet étudiant ambitieux se déroulant sur une année entière, de septembre 2021 à fin juin 2022. L'équipe est composée de  programmeurs, d'artistes et de game designers étudiant à l'UQAC et au NAD-UQAC.
            </h5>
            Occupant le rôle de programmeur,  ma tâche principale a été l'apparition et le comportement des enemis, des sortes de tourelles ayant la capacité d'entrer dans le sol pour en resortir plus loin.
        </div>
        <div class="col-3 p-3 mb-2 bg-dark text-right">
            de septembre 2021 à mai 2022<br>
            <h4>Unreal <img alt="voidscape-logo" height="20" src="LogoUnrealClair.png" /><br></h4>
            Projet de cours <br>
            20 personnes environ
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center m-2">
            Pour cela, il m'a fallu mettre en place un système de graphe pondéré pour lier les zones autorisée pour les monstres. Le but est d'obtenir un comportement d'essaim. En effet l'attitude première de ces créatures est de s'éloigner de leur point d'apparition afin de se répandre dans la pièce occupée, puis d'y revenir ils sont blessés.<br>
            Afin de faciliter une répartition plus homogène, la pondération des noeuds prends en compte la distance au point d'apparition, et le nombre de créatures qui s'y trouvent.
        </div>
    </div>
    <div class="row">
        <div class="col-6 text-center">
            <p class="text-center showcase">
                <br>
                <img class="img-fluid" alt="Graphe Shroom" src="shroomGraph.png" />
                <div class="m-2">Le graphe mis en place pour le déplacement des ennemis, avec la pondération de la distance aux noeuds d'apparition.</div>
            </p>
        </div>
        <div class="col-6 text-center">
            <p class="text-center showcase">
                <br>
                <img class="img-fluid" alt="Monstre en déplacement dans le graphe" src="shroomMovingSnips.png" />
                <div class="m-2">Les monstres en déplacement dans le graphe.</div>
            </p>
        </div>
    </div>
</div>
{{< /blocks/section >}}