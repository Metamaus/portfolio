---
title: "NGC 6705"
linkTitle: "NGC 6705"
weight: 10
type: _default
resources:
- src: "**.{png,jpg}"
---

{{< blocks/cover title="" image_anchor="bottom" height="max" color="yellow" >}}
<div class="row">
  <div class="col align-self-center">    
    <p class="text-center showcase">
        <img alt="NGC6705 ou l'amas du canard sauvage" height="350" src="ngclogo.png" />
    </p>
  </div>
</div>

## [**page itch**](https://metamaus.itch.io/ngc6705)
{{< /blocks/cover >}}

{{< blocks/section color="yellow">}}
<div class="container-md">
    <div class="row align-items-center">
        <div class="col-10 text-center">
            <h4>
            NGC6705 est un projet centré autour du réseau, réalisé à l'aide de la dernière solution réseau de Unity, <b>Netcode for GameObjects</b>.
            Dans ce projet, j'ai implémenté le ramassage des objets, le menu et aidé les autres programmeurs à réaliser leurs tâches.
            </h4>
        </div>
        <div class="col-2 p-3 mb-2 bg-dark text-right text-light">
            décembre 2021<br>
            <h4>Unity <img alt="voidscape-logo" height="20" src="LogoUnityClair.png" /><br></h4>
            Projet de cours <br>
            3 personnes
        </div>
    </div>
    <div class="gap-3">
        <div class="row ![img.png](img.png)mb3">
            <div class="col-12 text-center">
                <h3><br><b>Game Design</b></h3>
                <div>Comme nous avions peu de temps, nous nous sommes concentrés sur un jeu très simple. Deux canards doivent ramasser plus d'étoiles que leur adversaire en un temps limité, tout en évitant des éclairs.<br></div> 
            </div>
        </div>
        <div class="row m-3">
            <div class="col-6 text-center showcase">
                <img class="img-fluid" alt="NGC6705 en jeu" src="NGCInGame.png" />
                L'interaction avec les objets consiste tout simplement en une requête vers le serveur et une animation.
            </div>
            <div class="col-6 text-center showcase">
                <img class="img-fluid" alt="Le menu de NGC6705" src="NGCmenu.png" />
                Le menu gère la création de partie et la connexion, ce qui a réprésenté une véritable difficulté afin de vérifier que la connexion est valable.
            </div>
        </div>
    </div>
</div>
{{< /blocks/section >}}