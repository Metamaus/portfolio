---
title: A propos de moi
linkTitle: A propos de moi
menu:
  main:
    weight: 20
    pre: <i class="fas fa-dice-d20"></i>
---


{{< blocks/cover title="A propos de moi" image_anchor="bottom" height="min" >}}
{{< /blocks/cover >}}

{{% blocks/lead %}}
<div>
  Je suis un étudiant en recherche de stage en tant que programmeur gameplay. <br>
  Pour plus de détails sur mon parcours et mes centres d'intérêts, vous pouvez lire la suite.
</div>
{{% /blocks/lead %}}


{{< blocks/section color="dark">}}
<div class="container-sm">
  <div class="col-12 text-center">
    <h1>Parcours étudiant</h1>
    <h2>Classe préparatoire</h2>
    Durant 2 ans j'ai suivi une classe préparatoire en math et physique, et j'ai particulièrement apprécié la forte composante logique de la formation, à la fois dans l'approche des mathématiques mais aussi de l'informatique.<br>
    <h2>Ecole d'ingénieur</h2>
    J'ai ensuite rejoint l'ENSEIRB-MATEMCA en filière télécommunications. Cela m'a permis de pratiquer dans de nombreux domaines: la programmation réseau, système, le traitement d'image... L'intérêt pour moi était de découvrir la diversité d'outils disponibles sous le noms générique de "programmation".<br>
    <h2>Double diplôme à l'UQAC</h2>
    Afin de compléter ma formation et de mettre en application les éléments divers de ma formation, j'ai décidé de réaliser un double-diplôme à l'Université du Québec à Chicoutimi, en spécialisation jeux vidéos. Ce domaine, qui en rassemble de nombreux autres, insufle dans le tout une créativité que je trouve bienvenue.
  </div>
</div>

{{< /blocks/section >}}



{{< blocks/section color="info">}}
<div class="container-md">
  <div class="col-12 text-center text-primary">
    <h1>Centres d'intérêts</h1>
    <h2>Jeux de rôles</h2>
    <div>
      Un loisir qui me tient beaucoup à coeur car il met à contribution ma curiosité naturelle et mon amour des univers alternatif est le jeu de rôles papier. J'aime particulièrement le rôle de maître du jeu, conteur qui emporte les autres personnes autour de la table dans d'autres mondes.<br>
      Pour plus de détail, j'ai occupé ce rôle pour différents jeux: le classique Donjons et Dragons, l'Appel de cthulhu, le système Fate, l'irrévérencieux INS/MV et quelques systèmes créés à la main.
    </div>
    <h2>Jeux vidéos</h2>
    <div>
      Comme on peut le supposer d'après la catégorie précédente, je suis un grand amateur de rpg, et d'une manière plus générale de jeux à histoire. Je suis assez réservé sur les jeux de compétitions, si ce n'est pour jouer avec des amis, et de nombreux jeux indépendants tiennent une place à part dans mon coeur.
    </div>
    <h2>Dessin</h2>
    <div>
      Entre autre hobby, j'aime beaucoup dessiner et produire des représentations visuelles, que ce soit en dessin papier, à la tablette graphique ou à l'aide de mes faibles compétences en modélisation 3D.<br>
    </div>
  </div>
</div>
{{< /blocks/section >}}
