---
title: Autres projets
linkTitle: Autres projets
menu:
    main:
        weight: 10
        pre: <i class="fas fa-file-code"></i>
gamePage: false
---
{{< blocks/cover color="dark" height="min" >}}
<h1> Mes autres projets </h1>
{{% /blocks/cover %}}

{{< blocks/section color="secondary" >}}
<div class="row">
    <div class="col">
        <h2>Bibliothèque réseau en C++</h2>
        <div>
            Une bibliothèque réseau implémentant la plupart des fonctionnalités réseau nécessaire à un jeux vidéo.<br>
            Réalisé en C++.
        </div>
        <a href="https://gitlab.com/8inf9121-bfr">dépôt</a>
    </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="dark" >}}
<div class="row">
    <div class="col">
        <h2>Breakbot</h2>
        <div>
            Un bot github qui utilise l'outil Maracas pour détecter les changements problématiques dans des librairies logicielles et envoie des rapports à chaque pull request.<br>
            Réalisé en typescript.
        </div>
        <a href="https://github.com/alien-tools/breakbot">dépôt</a>
    </div>
</div>
{{% /blocks/section %}}

{{< blocks/section color="blue" >}}
<div class="row">
    <div class="col">
    <h2>Jeco</h2>
    <div>
        Un site web qui met en place des systèmes de classements alternatifs pour des jeux de compétition connus.<br>
        Travail sur la théorie des classements.
    </div>
    <a href="https://github.com/Enseirb-JECO">dépôt</a>
    </div>
</div>
{{% /blocks/section %}}